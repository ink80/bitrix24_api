# -*- coding: utf-8 -*-
import requests
import const
from multidimensional_urlencode import urlencode
# import json
# from flask import Flask, request, redirect

app_bitrix_name = const.app_bitrix_name
app_bitrix_url = const.app_bitrix_url
bitrix_client_id = const.bitrix_client_id
bitrix_secret_key = const.bitrix_secret_key
bitrix_domain = const.bitrix_domain

access_token = const.access_token


class BitrixChatBot:

    """
        Методы для работы с чатботами

    """

    @staticmethod
    def imbot_register(bot_code, event_handler, bot_name):

        """Метод регистрации нового чатбота битрикс"""

        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.register.json?&{auth}'
        params = {'CODE': bot_code,  # Строковой идентификатор бота, уникальный в рамках вашего приложения (обяз.)
                  'TYPE': 'B',
                  'EVENT_HANDLER': event_handler,  # Ссылка на обработчик события отправки
                  'OPENLINE': 'Y',  # // Включение режима поддержки Открытых линий, можно не указывать, если TYPE = 'O'
                  'CLIENT_ID': bot_code,
                  # // строковый идентификатор чат-бота, используется только в режиме Вебхуков
                  'PROPERTIES': {
                      'NAME': bot_name,  # // Имя чат-бота (обязательное одно из полей NAME или LAST_NAME)
                      'COLOR': 'GREEN',
                      'EMAIL': 'test@list.ru',
                      # // E-mail для связи. НЕЛЬЗЯ использовать e-mail, дублирующий e-mail реальных пользователей
                      'PERSONAL_BIRTHDAY': '2020-04-23',  # // День рождения в формате YYYY-mm-dd
                      'WORK_POSITION': 'Salaga',  # // Занимаемая должность, используется как описание чат-бота
                      'PERSONAL_WWW': 'http://test.ru',  # // Ссылка на сайт
                      'PERSONAL_GENDER': 'M',
                      'PERSONAL_PHOTO': const.icon_enable,  # // Аватар чат-бота - base64
                  }}
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            return f'Ошибка регистрации нового чатбота Битрикс {e}'

    @staticmethod
    def imbot_unregister(bot_id, bot_code):
        """Метод для удаления зарегистрированного чатбота битрикс"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.unregister.json?&{auth}'
        params = {
            'BOT_ID': int(bot_id),
            'CLIENT_ID': str(bot_code)
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка удаления бота с id:{bot_id}:   {e}")

    @staticmethod
    def imbot_update(bot_id, bot_code, event_handler, bot_name):

        """Метод для обновления данных чатбота битрикс"""

        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.update.json?&{auth}'
        params = {'BOT_ID': bot_id,  # Строковой идентификатор бота, уникальный в рамках вашего приложения (обяз.)
                  'FIELDS': {
                            'CODE': bot_code,
                            'TYPE': 'B',
                            'OPENLINE': 'Y',
                            'CLIENT_ID': bot_code,
                            'EVENT_HANDLER': event_handler,  # Ссылка на обработчик события отправки
                            # // строковый идентификатор чат-бота, используется только в режиме Вебхуков
                            'PROPERTIES': {
                                            'NAME': bot_name,
                                            'COLOR': 'GREEN',
                                            'EMAIL': 'test@list.ru',  # // E-mail для связи. НЕЛЬЗЯ
                                                                      # использовать e-mail, дублирующий e-mail
                                                                      # реальных пользователей
                                            'PERSONAL_BIRTHDAY': '2020-04-23',  # // День рождения в формате YYYY-mm-dd
                                            'WORK_POSITION': 'Мой первый бот',  # // Занимаемая должность,
                                                                                # используется как описание чат-бота
                                            'PERSONAL_WWW': 'http://test.ru',  # // Ссылка на сайт
                                            'PERSONAL_GENDER': 'M',
                                            'PERSONAL_PHOTO': const.icon_enable,  # // Аватар чат-бота - base64
                            }}}
        try:
            response = requests.post(url, json=params)
            return response.text

        except Exception as e:
            return f'Ошибка обновления данных чатбота Битрикс {e}'

    @staticmethod
    def imbot_bot_list():
        """Метод для получения списка  зарегистрированных чатбота битрикс"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.bot.list.json?&{auth}'
        try:
            response = requests.get(url)
            return response.json()
        except Exception as e:
            print(f"Ошибка получения списка зарегистрированных в битрикс чатботов:   {e}")

    """
        Методы для работы с чатами

    """

    @staticmethod
    def imbot_chat_add(chat_title, chat_desc, chat_color, first_message, users):
        """Метод для создания нового чата битрикс от лица чатбота"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.add.json?&{auth}'
        params = {
            'TYPE': 'CHAT',
            # OPEN - открытый  для вступления чат, CHAT - обычный чат по приглашениям, по - умолчанию CHAT
            'TITLE': chat_title,  # Заголовок чата
            'DESCRIPTION': chat_desc,  # Описание чата
            'COLOR': chat_color,
            'MESSAGE': first_message,  # Первое  приветственное сообщение в чате
            'USERS': users  # Участники чата(обяз.) список [1, 2]
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка добавления нового чата {e}")

    @staticmethod
    def imbot_chat_user_list(bot_id, chat_id):
        """Метод для получения списка участников чата"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.user.list.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'CHAT_ID': chat_id
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка получения списка участников чата {e}")

    @staticmethod
    def imbot_chat_user_delete(bot_id, chat_id, chat_user_id):
        """Метод для сключения юзера из списка участников чата"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.user.delete.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'CHAT_ID': chat_id,
            'USER_ID': chat_user_id
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка удаления юзера из списка участников чата {e}")

    @staticmethod
    def imbot_chat_leave(bot_id, chat_id):
        """Метод для выхода чат бота из указанного чата"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.leave.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'CHAT_ID': chat_id
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка выхода чат бота из указанного чата {e}")

    @staticmethod
    def imbot_chat_get(bot_id, entity_id):
        """Метод для получения идентификатора чата чата"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.get.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'ENTITY_TYPE': 'OPEN',  # Идентификатор произвольной сущности (например CHAT, CRM, OPENLINES, CALL и тд),
                                    # может быть использован для поиска чата и для легкого определения контекста в
                                    # обработчиках событий ONIMBOTMESSAGEADD, ONIMBOTMESSAGEUPDATE,
                                    # ONIMBOTMESSAGEDELETE (обяз.)
            'ENTITY_ID': entity_id  # Числовой идентификатор сущности, может быть использован для поиск чата и для
                                    # легкого определения контекста в обработчиках событий ONIMBOTMESSAGEADD,
                                    # ONIMBOTMESSAGEUPDATE, ONIMBOTMESSAGEDELETE (обяз.)
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка получения идентификатора чата чата {e}")

    """
        Методы для работы с сообщениями

    """

    @staticmethod
    def send_message_to_bot(bot_id, dialog_id, text):
        """Метод для отправки сообщения от имени бота"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.message.add.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'DIALOG_ID': dialog_id,
            'MESSAGE': text
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка отправки сообщения в диалог с ботом {e}")

    @staticmethod
    def imbot_message_delete(dialog_id, message_id):
        """Метод для удаления сообщения"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.message.delete.json?&{auth}'
        params = {
            'DIALOG_ID': dialog_id,
            'MESSAGE_ID': message_id,
            'COMPLETE': 'N',  # Если сообщение нужно удалить полностью, без следов,
                              # то необходимо указать Y(необязательный параметр)
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка удаления сообщения {e}")

    @staticmethod
    def imbot_message_update(dialog_id, message_id):
        """Метод для обновления сообщения"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.message.update.json?&{auth}'
        params = {
            'DIALOG_ID': dialog_id,
            'MESSAGE_ID': message_id,
            'MESSAGE': 'answer text',  # Текст сообщения, необязательное поле,
                                       # если передать пустое значение - сообщение будет удалено
            'ATTACH': '',  # Вложение, необязательное поле
            'KEYBOARD': '',  # Клавиатура, необязательное поле
            'MENU': '',  # Контекстное меню, необязательное поле
            'URL_PREVIEW': 'Y'  # Преобразовывать ссылки в rich-ссылки, необязательное поле
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка обновления сообщения {e}")

    @staticmethod
    def imbot_chat_sendtyping(bot_id, dialog_id):
        """Отправка сообщения «Чат-бот пишет сообщение...»"""
        auth = urlencode({'auth': const.access_token})
        url = f'{bitrix_domain}/rest/imbot.chat.sendTyping.json?&{auth}'
        params = {
            'BOT_ID': bot_id,
            'DIALOG_ID': dialog_id
        }
        try:
            response = requests.post(url, json=params)
            return response.text
        except Exception as e:
            print(f"Ошибка отправки сообщения «Чат-бот пишет сообщение...» {e}")

